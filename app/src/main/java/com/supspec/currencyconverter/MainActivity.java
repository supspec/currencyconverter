package com.supspec.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public void convertCurrency(View view){
        //Log.i("Info", "Button pressed!");
        EditText editText = (EditText) findViewById(R.id.editText);
        Log.i("Info", editText.getText().toString());
        try {
            double amountInDollars = Double.parseDouble(editText.getText().toString());
            double amountInRible = amountInDollars*64.44;

            Toast toast = Toast.makeText(this, String.format("%.2f RUB", amountInRible), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();


        }catch (NumberFormatException e){
            Log.i("Info", "Wrong amount format!");
            Toast toast = Toast.makeText(this, "Wrong amount format", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
